package com.imooc.enums;

/**
 * 性别枚举
 *
 * @author Administrator
 */

public enum Sex {
    WOMAN (0,"女"),
    MAN (1,"男"),
    SECRET (2,"保密");


    private final Integer type;
    private final String value;

    Sex(Integer type, String value) {
        this.type = type;
        this.value = value;
    }

    public Integer getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
