package com.imooc.mapper;

import com.imooc.my.mapper.MyMapper;
import com.imooc.pojo.Category;
import com.imooc.pojo.vo.CategoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface CategoryMapperCustom  {
    /**
     * 获取所有子分类
     * @param rootCatId
     * @return
     */
    List<CategoryVO> getSubCatList(Integer rootCatId);

    /**
     * 查询最新六条商品信息
     * @param map
     * @return
     */
    List getSixNexItemsLazy(@Param("paramsMap")Map<String,Object> map);
}