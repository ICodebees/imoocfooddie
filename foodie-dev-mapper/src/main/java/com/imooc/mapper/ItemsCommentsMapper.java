package com.imooc.mapper;

import com.imooc.my.mapper.MyMapper;
import com.imooc.pojo.ItemsComments;

/**
 * @author Administrator
 */
public interface ItemsCommentsMapper extends MyMapper<ItemsComments> {
}