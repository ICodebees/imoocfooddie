package com.imooc.mapper;

import com.imooc.pojo.vo.ItemCommentVO;
import com.imooc.pojo.vo.SearchItemsVO;
import com.imooc.pojo.vo.ShopCartVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface ItemsMapperCustom {
    /**
     * 获取评价
     *
     * @param paramsMap
     * @return
     */
    List<ItemCommentVO> queryItemComments(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 搜索商品
     *
     * @param paramsMap
     * @return
     */
    List<SearchItemsVO> searchItems(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 三级分类搜索
     *
     * @param paramsMap
     * @return
     */
    List<SearchItemsVO> searchItemsByThirdCat(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 根据specId搜索
     * @param paramsList
     * @return
     */
    List<ShopCartVO>queryItemsBySpecIds(@Param("paramsList")List  paramsList);
}
