package com.imooc.controller;

import com.imooc.service.UserService;
import com.imooc.utils.IMOOCJSONResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("passport")
public class PassPortController {
    @Resource
    UserService userService;

    @GetMapping("/usernameIsExist")
    public IMOOCJSONResult userNameIsExist(@RequestParam String username) {

        //判断入参是否为空
        if (StringUtils.isBlank(username)) {
            return IMOOCJSONResult.errorMsg("输入用户名为空");
        }
        //查询注册的用户名是否存在
        boolean userNameExist = userService.queryUsernameIsExist(username);
        if (userNameExist) {
            return IMOOCJSONResult.errorMsg("用户名已存在");
        }
        //请求成功,用户名没有重复
        return IMOOCJSONResult.ok();
    }
}
