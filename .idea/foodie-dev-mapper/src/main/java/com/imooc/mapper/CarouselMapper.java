package com.imooc.mapper;

import com.imooc.my.mapper.MyMapper;
import com.imooc.pojo.Carousel;

/**
 * @author Administrator
 */
public interface CarouselMapper extends MyMapper<Carousel> {
}