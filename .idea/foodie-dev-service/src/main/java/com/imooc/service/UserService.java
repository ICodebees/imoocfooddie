package com.imooc.service;

/**
 * @author Administrator
 */
public interface UserService {
    /**
     * 查看用户是否存在
     *
     * @param username
     * @return true or false
     */
    boolean queryUsernameIsExist(String username);
}
