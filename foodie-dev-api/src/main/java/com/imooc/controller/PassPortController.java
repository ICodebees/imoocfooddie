package com.imooc.controller;

import com.imooc.pojo.Users;
import com.imooc.pojo.bo.UserBO;
import com.imooc.service.UserService;
import com.imooc.utils.CookieUtils;
import com.imooc.utils.IMOOCJSONResult;
import com.imooc.utils.JsonUtils;
import com.imooc.utils.MD5Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Administrator
 */
@Api(value = "注册登录", tags = {"用于注册相关操作"})
@RestController
@RequestMapping("passport")
public class PassPortController {
    @Resource
    UserService userService;

    @ApiOperation(value = "用户名是否存在", httpMethod = "GET")
    @GetMapping("/usernameIsExist")
    public IMOOCJSONResult userNameIsExist(@RequestParam String username) {

        //判断入参是否为空
        if (StringUtils.isBlank(username)) {
            return IMOOCJSONResult.errorMsg("输入用户名为空");
        }
        //查询注册的用户名是否存在
        boolean userNameExist = userService.queryUsernameIsExist(username);
        if (userNameExist) {
            return IMOOCJSONResult.errorMsg("用户名已存在");
        }
        //请求成功,用户名没有重复
        return IMOOCJSONResult.ok();
    }

    @ApiOperation(value = "注册", httpMethod = "POST")
    @PostMapping("/register")
    public IMOOCJSONResult register(@RequestBody UserBO userBO,
                                    HttpServletRequest request,
                                    HttpServletResponse response) {
        String username = userBO.getUsername();
        String password = userBO.getPassword();
        String confirmPassword = userBO.getConfirmPassword();
        //判断用户名和密码不为空
        if (StringUtils.isBlank(username) ||
                StringUtils.isBlank(password) ||
                StringUtils.isBlank(confirmPassword)) {
            return IMOOCJSONResult.errorMsg("用户名密码不能为空");
        }
        boolean exist = userService.queryUsernameIsExist(username);
        //查询用户名是否存在
        if (exist) {
            return IMOOCJSONResult.errorMsg("用户名存在");
        }
        //密码长度不少于6位
        if (password.length() < 6) {
            return IMOOCJSONResult.errorMsg("密码小于6位");
        }
        //判断两次密码是否一致
        if (!password.equals(confirmPassword)) {
            return IMOOCJSONResult.errorMsg("两次密码不同");
        }
        //实现注册
        Users user = userService.createUser(userBO);
        user = setNullProperty(user);
        CookieUtils.setCookie(request,
                response,
                "user",
                JsonUtils.objectToJson(user), true);
        return IMOOCJSONResult.ok();
    }

    @ApiOperation(value = "登录", httpMethod = "POST")
    @PostMapping("/login")
    public IMOOCJSONResult login(@RequestBody UserBO userBO,
                                 HttpServletRequest httpServletRequest,
                                 HttpServletResponse httpServletResponse) throws Exception {
        String username = userBO.getUsername();
        String password = userBO.getPassword();
        //判断用户名和密码不为空
        if (StringUtils.isBlank(username) ||
                StringUtils.isBlank(password)) {
            return IMOOCJSONResult.errorMsg("用户名密码不能为空");
        }
        //实现登录
        Users user = userService.queryUserForLogin(username, MD5Utils.getMD5Str(password));
        if (user == null) {
            return IMOOCJSONResult.errorMsg("用户名密码不正确");
        }
        user = setNullProperty(user);
        CookieUtils.setCookie(httpServletRequest,
                httpServletResponse,
                "user",
                JsonUtils.objectToJson(user), true);
        //TODO 生成用户Token,存入redis缓存
        //TODO 同步购物车数据
        return IMOOCJSONResult.ok(user);
    }

    private Users setNullProperty(Users user) {
        user.setPassword(null);
        user.setMobile(null);
        user.setEmail(null);
        user.setCreatedTime(null);
        user.setUpdatedTime(null);
        user.setBirthday(null);
        return user;
    }

    @ApiOperation(value = "用户退出登录", httpMethod = "POST")
    @PostMapping("/logout")
    public IMOOCJSONResult logout(@RequestParam String userId,
                                  HttpServletRequest request,
                                  HttpServletResponse response) {
        CookieUtils.deleteCookie(request, response, "user");
        //TODO 用户退出登录,需要清楚购物车
        //TODO 分布式会话中需要清除用户数据
        return IMOOCJSONResult.ok();
    }
}
