package com.imooc.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * @author Administrator
 */
@Component
@Aspect
public class ServiceLogAspect {
    private static final Logger log =
            LoggerFactory.getLogger(ServiceLogAspect.class);

    /**
     * AOP通知
     * 1.前置通知
     * 2.后置通知
     * 3.环绕通知
     * 4.异常通知
     * 5.最终通知
     */

    /**
     * 日志打印切面方法
     * 切面表达式
     * execution 代表要执行的表达式主体
     * 第一处 * 代表方法返回类型,*表示所有
     * 第二处 表示aop切入包的位置
     * 第三处 .. 表示该包及其子包
     * 第四处 * 表示所有类
     * 第五处 *(..) *表示类的所用方法(..)表示所有参数
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("execution(* com.imooc.service.impl..*.*(..))")
    public Object recordTimeLog(ProceedingJoinPoint joinPoint) throws Throwable {
        //打印切点的class和执行方法
        log.info("========= 开始执行{}.{} ==========",
                joinPoint.getTarget().getClass(),
                joinPoint.getSignature().getName());
        //记录开始时间
        long begin = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        //记录结束时间
        long end = System.currentTimeMillis();
        long takeTime = end - begin;
        if (takeTime > 3000) {
            log.error("======== 执行结束, 耗时{}毫秒", takeTime);
        }
        if (takeTime > 2000) {
            log.warn("======== 执行结束, 耗时{}毫秒", takeTime);
        } else {
            log.info("======== 执行结束, 耗时{}毫秒", takeTime);
        }
        return result;
    }

}
