package com.imooc.service;

import com.imooc.pojo.Category;
import com.imooc.pojo.vo.CategoryVO;

import java.util.List;

/**
 * @author Administrator
 */
public interface CategoryService {

    /**
     * 查询所有一级分类
     * @return
     */
    List<Category> queryAllRootLevelCat();
    /**
     * 查询二级分类
     * @param rootCatId
     * @return
     */
    List<CategoryVO>getSubCatList(Integer rootCatId);

    /**
     * 查询最新六条商品信息
     * @param rootCatId
     * @return
     */
    List getSixNexItemsLazy(Integer rootCatId);
}
