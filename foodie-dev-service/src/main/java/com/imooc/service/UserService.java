package com.imooc.service;

import com.imooc.pojo.Users;
import com.imooc.pojo.bo.UserBO;

/**
 * @author Administrator
 */
public interface UserService {
    /**
     * 查看用户是否存在
     *
     * @param username
     * @return true or false
     */
    boolean queryUsernameIsExist(String username);

    /**
     * 创建user
     * @param userBO
     * @return users
     */
    Users createUser(UserBO userBO);

    /**
     * 验证登录
     * @param username
     * @param password
     * @return
     */
    Users queryUserForLogin(String username,String password);


}
