package com.imooc.service;

import com.imooc.pojo.*;
import com.imooc.pojo.vo.CommentLevelCountsVO;
import com.imooc.pojo.vo.ItemCommentVO;
import com.imooc.pojo.vo.ShopCartVO;
import com.imooc.utils.PagedGridResult;

import java.util.List;

/**
 * @author Administrator
 */
public interface ItemService {
    /**
     * 根据ID获取商品详情
     *
     * @param itemId
     * @return
     */
    Items queryItemById(String itemId);

    /**
     * 获取商品所有相关图片
     *
     * @param id
     * @return
     */
    List<ItemsImg> queryItemImgList(String id);

    /**
     * 获取商品所有规格
     *
     * @param id
     * @return
     */
    List<ItemsSpec> queryItemSpecList(String id);

    /**
     * 查询商品参数
     * @param id
     * @return
     */
    ItemsParam queryItemParam(String id);

    /**
     * 根据商品id查询商品的评价等级数量
     * @param itemId
     * @return
     */
    CommentLevelCountsVO queryCommentCount(String itemId);

    /**
     * 分页查询评论
     * @param itemId
     * @param level
     * @param page
     * @param pageSize
     * @return
     */
    PagedGridResult queryPageComments(String itemId, Integer level, Integer page, Integer pageSize);

    /**
     *  搜索商品列表
     * @param keywords
     * @param sort
     * @param page
     * @param pageSize
     * @return
     */
    PagedGridResult searchItems(String keywords, String sort, Integer page, Integer pageSize);

    /**
     * 三级搜索
     * @param catId
     * @param sort
     * @param page
     * @param pageSize
     * @return
     */
    PagedGridResult searchItemsByThirdCat(Integer catId, String sort, Integer page, Integer pageSize);

    /**
     * 根据规格ids查询最新的购物车中商品数据
     * @param specIds
     * @return
     */
    List<ShopCartVO>queryItemsBySpecIds(String specIds);
}
