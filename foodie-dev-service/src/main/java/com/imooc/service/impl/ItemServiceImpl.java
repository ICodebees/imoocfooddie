package com.imooc.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imooc.enums.CommentLevel;
import com.imooc.mapper.*;
import com.imooc.pojo.*;
import com.imooc.pojo.vo.CommentLevelCountsVO;
import com.imooc.pojo.vo.ItemCommentVO;
import com.imooc.pojo.vo.SearchItemsVO;
import com.imooc.pojo.vo.ShopCartVO;
import com.imooc.service.ItemService;
import com.imooc.utils.DesensitizationUtil;
import com.imooc.utils.PagedGridResult;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Administrator
 */
@Service
public class ItemServiceImpl implements ItemService {
    @Resource
    private ItemsMapper itemsMapper;
    @Resource
    private ItemsImgMapper itemsImgMapper;
    @Resource
    private ItemsSpecMapper itemsSpecMapper;
    @Resource
    private ItemsParamMapper itemsParamMapper;
    @Resource
    private ItemsCommentsMapper itemsCommentsMapper;
    @Resource
    private ItemsMapperCustom itemsMapperCustom;

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public Items queryItemById(String itemId) {
        Example example = new Example(Items.class);
        final Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", itemId);
        final Items item = itemsMapper.selectOneByExample(example);
        return item;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List<ItemsImg> queryItemImgList(String id) {
        Example example = new Example(ItemsImg.class);
        final Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("itemId", id);
        final List<ItemsImg> itemsImgs = itemsImgMapper.selectByExample(example);

        return itemsImgs;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List<ItemsSpec> queryItemSpecList(String id) {
        Example example = new Example(ItemsSpec.class);
        final Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("itemId", id);
        final List<ItemsSpec> itemsSpecs = itemsSpecMapper.selectByExample(example);
        return itemsSpecs;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public ItemsParam queryItemParam(String id) {
        Example example = new Example(ItemsParam.class);
        final Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("itemId", id);
        final ItemsParam itemsParam = itemsParamMapper.selectOneByExample(example);
        return itemsParam;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public CommentLevelCountsVO queryCommentCount(String itemId) {
        CommentLevelCountsVO commentLevelCountsVO = new CommentLevelCountsVO();
        Integer badCommentCounts = getCommentCounts(itemId, CommentLevel.BAD);
        commentLevelCountsVO.setBadCounts(badCommentCounts);
        Integer normalCommentCounts = getCommentCounts(itemId, CommentLevel.NORMAL);
        commentLevelCountsVO.setNormalCounts(normalCommentCounts);
        Integer goodCommentCounts = getCommentCounts(itemId, CommentLevel.GOOD);
        commentLevelCountsVO.setGoodCounts(goodCommentCounts);
        Integer total = badCommentCounts + goodCommentCounts + normalCommentCounts;
        commentLevelCountsVO.setTotalCounts(total);
        return commentLevelCountsVO;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    Integer getCommentCounts(String itemId, CommentLevel level) {
        ItemsComments condition = new ItemsComments();
        condition.setItemId(itemId);
        if (level != null) {
            condition.setCommentLevel(level.getType());
        }
        int result = itemsCommentsMapper.selectCount(condition);
        return result;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public PagedGridResult queryPageComments(String itemId,
                                             Integer level,
                                             Integer page,
                                             Integer pageSize) {
        final HashMap<String, Object> condition = new HashMap<>();
        condition.put("itemId", itemId);
        condition.put("level", level);
        PageHelper.startPage(page, pageSize);
        List<ItemCommentVO> result = itemsMapperCustom.queryItemComments(condition);
        for (ItemCommentVO itemCommentVO : result) {
            itemCommentVO.setNickName(DesensitizationUtil.commonDisplay(itemCommentVO.getNickName()));
        }
        PagedGridResult grid = setterPagedGrid(result, page);
        return grid;
    }

    private PagedGridResult setterPagedGrid(List<?> result, Integer page) {
        PageInfo<?> pageList = new PageInfo<>(result);
        PagedGridResult grid = new PagedGridResult();
        grid.setPage(page);
        grid.setRows(result);
        grid.setRecords(pageList.getTotal());
        grid.setTotal(pageList.getPages());
        return grid;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public PagedGridResult searchItems(String keywords, String sort, Integer page, Integer pageSize) {
        HashMap<String, Object> condition = new HashMap<>();
        condition.put("keywords", keywords);
        condition.put("sort", sort);
        PageHelper.startPage(page, pageSize);
        List<SearchItemsVO> result = itemsMapperCustom.searchItems(condition);
        PagedGridResult grid = setterPagedGrid(result, page);
        return grid;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public PagedGridResult searchItemsByThirdCat(Integer catId, String sort, Integer page, Integer pageSize) {
        HashMap<String, Object> condition = new HashMap<>();
        condition.put("catId", catId);
        condition.put("sort", sort);
        PageHelper.startPage(page, pageSize);
        List<SearchItemsVO> result = itemsMapperCustom.searchItemsByThirdCat(condition);
        PagedGridResult grid = setterPagedGrid(result, page);
        return grid;
    }
    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List<ShopCartVO> queryItemsBySpecIds(String specIds) {
        String[] ids = StringUtils.split(specIds, ',');
        List<String> specIdsList = new ArrayList<>();
        Collections.addAll(specIdsList,ids);
        List<ShopCartVO> result = itemsMapperCustom.queryItemsBySpecIds(specIdsList);
        return result;
    }
}
