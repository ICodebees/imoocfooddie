package com.imooc.service.impl;

import com.imooc.mapper.CategoryMapper;
import com.imooc.mapper.CategoryMapperCustom;
import com.imooc.pojo.Category;
import com.imooc.pojo.vo.CategoryVO;
import com.imooc.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author Administrator
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private CategoryMapperCustom categoryMapperCustom;

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List<Category> queryAllRootLevelCat() {
        Example queryExample = new Example(Category.class);
        Example.Criteria condition = queryExample.createCriteria();
        condition.andEqualTo("type", 1);
        List<Category> results = categoryMapper.selectByExample(queryExample);
        return results;
    }

    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List<CategoryVO> getSubCatList(Integer rootCatId) {
        List results = categoryMapperCustom.getSubCatList(rootCatId);
        return results;
    }
    @Transactional(propagation = Propagation.SUPPORTS, rollbackFor = {})
    @Override
    public List getSixNexItemsLazy(Integer rootCatId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("rootCatId",rootCatId);
        List results = categoryMapperCustom.getSixNexItemsLazy(map);
        return results;
    }
}
