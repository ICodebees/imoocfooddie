package com.imooc.service.impl;

import com.imooc.mapper.CarouselMapper;
import com.imooc.pojo.Carousel;
import com.imooc.service.CarouselService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Administrator
 */
@Service
public class CarouselServiceImpl implements CarouselService {

    @Resource
    private CarouselMapper carouselMapper;

    @Override
    public List<Carousel> queryAll(Integer isShow) {
        Example queryExample = new Example(Carousel.class);
        queryExample.orderBy("sort").desc();
        Example.Criteria condition = queryExample.createCriteria();
        condition.andEqualTo("isShow", isShow);
        List<Carousel> results = carouselMapper.selectByExample(queryExample);
        return results;
    }
}
